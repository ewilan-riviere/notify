"""Define Discord server"""


class Server:
    """Discord server class"""
    name: str
    server_id: str
    server_token: str
    url: str

    def __init__(self, name: str, server_id: str, server_token: str):
        self.name = name
        self.server_id = server_id
        self.server_token = server_token
        self.url = 'https://discord.com/api/webhooks/'
        self.url += server_id + '/' + server_token

    def __str__(self):
        return self.name
