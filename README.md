# Notify

[![python][version-src]][version-href]

Script to send notifications on Discord.

## Setup

Python dependencies

```bash
pip install python-dotenv requests
```

If you want to install system-wide, use `apt`:

```bash
sudo apt install python3-dotenv python3-requests
```

Copy `.env.example` to `.env` and add Discord servers.

```bash
cp .env.example .env
```

Add Discord servers to `.env` file with the following format for `SERVERS`: `NAME:SERVER_ID:SERVER_TOKEN`. You can add multiple servers by separating them with a comma.

```bash
# .env
SERVERS=NAME:SERVER_ID:SERVER_TOKEN,ANOTHER_NAME:SERVER_ID:SERVER_TOKEN
```

Create symlink to `notify` script.

```bash
sudo rm -f /usr/local/bin/notify
```

```bash
sudo ln -s /path/to/notify/notify /usr/local/bin
```

## Usage

```bash
notify NAME "Hey, I'm a notification!"
notify ANOTHER_NAME "Hey, I'm a notification on another server!"
```

## Test

Check with `curl` if the webhook is working.

```bash
curl -i -H "Accept: application/json" \
  -H "Content-Type:application/json" \
  -X POST --data \
  "{\"content\": \"Posted Via Command line\"}" \
  https://discord.com/api/webhooks/SERVER_ID/SERVER_TOKEN
```

## License

[MIT](LICENSE)

[version-src]: https://img.shields.io/static/v1?style=flat-square&label=Python&message=v3.x&color=3776AB&logo=python&logoColor=ffffff&labelColor=18181b
[version-href]: https://www.python.org/
