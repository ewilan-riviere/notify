

"""Send Discord notification with webhook"""
import sys
from dotenv import dotenv_values
import requests
import server as serverItem

args = sys.argv

ARG_DOTENV = args[1]
ARG_SERVER = args[2].upper()
ARG_MSG_RAW = args[3]
ARG_MSG = ARG_MSG_RAW.replace("_", " ")

config = dotenv_values(ARG_DOTENV)
servers = config['SERVERS']


def send_to_discord(item: serverItem.Server, msg: str) -> bool:
    """Send message to Discord"""
    if item.name == ARG_SERVER:
        data = {"content": msg}
        requests.post(
            item.url,
            json=data,
            headers={"Content-Type": "application/json"},
            timeout=5
        )

        return True

    return False


if servers is not None:
    servers = servers.split(',')
else:
    servers = []


serverList: list[serverItem.Server] = []
for server in servers:
    values = server.split(':')
    server = values[0]
    serverId = values[1]
    serverToken = values[2]

    server = serverItem.Server(server, serverId, serverToken)
    serverList.append(server)

success: bool = False
for server in serverList:
    success = send_to_discord(server, ARG_MSG)
    break

if success is False:
    print('Server not found')
